\documentclass[11pt]{article}
\pagestyle{plain}
\usepackage[margin=1.0in,tmargin=0.75in,bmargin=0.75in]{geometry}
\usepackage{fancyhdr}
\usepackage{multicol} % For 2 col itemize
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\rfoot{\thepage}

\usepackage{amsmath,amssymb,graphicx,amsthm}
\usepackage[table,xcdraw]{xcolor}

\usepackage{pgfplots}
\pgfplotsset{compat=1.13}

\newcommand{\tf}{[T/F] }
\newcommand{\bfN}{\mathbf{N}}

\usepackage[T1]{fontenc}
\usepackage{mathpazo} 
\setlength{\parindent}{0em}
\setlength{\parskip}{\baselineskip}

\DeclareMathOperator{\Span}{span}

\begin{document}
\title{Speed and Acceleration Lab}
\section*{Integrated Science: Speed and Acceleration Lab \hfill 3.1}
%Name: \underline{\hspace{3in}} \hfill Date: \underline{\hspace{1in}}
\par
Our goal is to discover science concepts in a safe, real world environment.
\bigskip
\hrule
\section*{Pre-laboratory Work}
Speed is defined as the distance an object travels per unit time.  Speed can also be expressed as kilometers per hour $\left( \frac{km}{h} \right)$, meters per second $\left( \frac{m}{s} \right)$, and so on.  In most cases, moving objects do not travel at a constant speed.  The speed of an object usually increases and decreases as the object moves.  Therefore, the average is used to describe the motion.  Average speed is a ratio between the total distance and the total time that the object traveled.
$$\text{average speed} = \frac{\text{total distance}}{\text{total time}}$$
Acceleration is the rate at which an object's speed increases.  You can express acceleration as meters per second per second $\left( \frac{m}{s^2} \right)$.  This unit represents the change in speed in meters per second each second.  Forces cause objects to accelerate and decelerate (decrease in the rate of speed).  If a car has an average speed of $80 \text{ }\frac{km}{h}$ on a hilly road, it probably changes speed many times.  This means that the car accelerates and decelerates.  If the car is traveling at a constant speed of $80\text{ } \frac{km}{h}$ on a level road, it is not changing speed which means that the acceleration of the car is zero.
\subsection*{Objectives}
In this experiment, you will:
\begin{itemize}
    \item determine the average speed of a small toy car
    \item study the forces that affect the motion of the car
    \item observe the deceleration of the car
    \item determine conditions that will not affect the speed of a moving object
\end{itemize}

\subsection*{Equipment}
\begin{multicols}{3}
\begin{itemize}
    \item Time keeping device
    \item Masking Tape
    \item Ramp (about 50 cm long)
    \item Meter stick
    \item Pen
    \item Toy car or ball
\end{itemize}
\end{multicols}

\subsection*{Procedure}
\subsubsection*{Part A - Average Speed}
\begin{enumerate}
    \item Clear a runway about 6 meters long.
    \item At one end of the runway, set up the launching ramp.  Set it up approximately 20 cm tall.
    \item Place a masking tape marker where the ramp touches the floor.  Label this marker 0.0 m.  Place labels at 1.0 m, 2.0 m, 3.0 m, 4.0 m, 5.0 m, 6.0 m.
    \item Practice launch the rolling object down the ramp several times.  Adjust the ramp so that the object travels a distance of 5.0 meters.
    \item Measure the time that the object takes to travel the 5.0 meters. Record the time and distance in your lab notebook (You may want to make a table similar to Table \ref{table:Data1}).  Measure and record the times of 4 more trials.  Make sure to record these in your lab notebook.
\end{enumerate}
\subsubsection*{Part B - Deceleration}
\begin{enumerate}
    \item Repeat steps 4 and 5 from Part A.  However, now measure the time required for the object to pass each marker.  You may require several practice runs to be able to observe and record the times quickly.  One lab partner can observe the times while another records the data.
    \item Complete four trials.  Record the times and distances in your lab notebook (it may be helpful to make a table similar to Table \ref{Table:Data2})
    \item Calculate the average speed of the object as it passes each marker.  Record the result to the nearest 0.1 $\frac{m}{s}$ in Table \ref{Table:Data2}.
    \item Make a graph to compare the average speed of the object ($y$ axis) to the distance to each marker ($x$ axis).
\end{enumerate}
\subsection*{Conclusions}
\begin{enumerate}
    \item Describe the motion of the car as it moved across the floor.
    \item What caused the car to slow down and stop?
    \item Did the object travel at a constant speed?  How do you know this?
    \item How could you change the experiment to make the toy car decelerate at a faster rate?
    \item How could you change this experiment to make the car accelerate at a faster rate?
    \item Consider the 5.0 meters that the car traveled.  What conditions are necessary for the car to have no acceleration or deceleration?
    \item How could you design an experiment to make the object decelerate quicker? 
\end{enumerate}

\begin{table}[]
\caption{Data Collection 1}
\centering
\begin{tabular}{|c|c|}
\hline
\rowcolor[HTML]{C0C0C0} 
Trial   & Time (s) \\ \hline
1       &          \\ \hline
2       &          \\ \hline
3       &          \\ \hline
4       &          \\ \hline
5       &          \\ \hline
Average &          \\ \hline
\end{tabular}
\label{table:Data1}
\end{table}

\begin{table}[]
\caption{Acceleration Data Collection}
\centering
\begin{tabular}{|c|c|l|l|l|l|}
\hline
\rowcolor[HTML]{C0C0C0} 
                                            & \multicolumn{5}{c|}{\cellcolor[HTML]{C0C0C0}Time (s)} \\ \hline
Trial                                       & 1.0 m                 & 2.0 m & 3.0 m & 4.0 m & 5.0 m \\ \hline
1                                           & \multicolumn{1}{l|}{} &       &       &       &       \\ \hline
2                                           &                       &       &       &       &       \\ \hline
3                                           &                       &       &       &       &       \\ \hline
4                                           &                       &       &       &       &       \\ \hline
5                                           &                       &       &       &       &       \\ \hline
Average                                     &                       &       &       &       &       \\ \hline
\multicolumn{1}{|l|}{Average Speed $\left( \frac{m}{s} \right)$} & \multicolumn{1}{l|}{} &       &       &       &       \\ \hline
\end{tabular}
\label{Table:Data2}
\end{table}
\end{document}